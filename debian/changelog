fonts-sil-abyssinica (2.000-1) unstable; urgency=medium

  * New upstream release.
  * Update packaging.
  * Do not strip the font of Latin characters for the Debian installer.
  * Update Standards to 4.4.1 (checked)

 -- Bobby de Vos <bobby_devos@sil.org>  Mon, 21 Oct 2019 07:49:12 -0600

fonts-sil-abyssinica (1.500-1) unstable; urgency=low

  * New upstream release. Closes: #747570
  * Update Standards to 3.9.5 (checked)

 -- Christian Perrier <bubulle@debian.org>  Sat, 10 May 2014 18:43:27 +0200

fonts-sil-abyssinica (1.200-6) unstable; urgency=low

  * Team upload

  [ Sebastian Bator ]
  * Convert the repository from svn to git
  * Add upstream and pristine-tar branch for use by git-buildpackage
  * Update Vcs-* Fields to reflect the use of git
  * Bump debhelper compatibility to 9
  * Drop ttf-sil-abyssinica, this mean further:
    fonts-sil-abyssinica now "Breaks" ttf-sil-abyssinica (instead of
    "Conflicts")
    ttf-sil-abyssinica is no longer provided by fonts-sil-abyssinica
  * Add "Multi-Arch: foreign" field
  * Update Standards to 3.9.4 (checked, no changes)
  * Removed first person in package description

  [ Christian Perrier ]
  * Replace XC-Package-Type by Package-Type in debian/control
  * Use the copyright-format 1.0 URL in debian/copyright
  * Use LF in debian/copyright instead of CRLF
  * Drop "Upstream Author:" in debian/copyright
  * Clean out debian/copyright so that it passes lintian tests

 -- Sebastian Bator <eremit@posteo.de>  Thu, 02 May 2013 18:22:50 +0200

fonts-sil-abyssinica (1.200-5) unstable; urgency=low

  * Team upload
  * Install fonts in abyssinica/ to align on packaging customs

 -- Sebastian Bator <eremit@posteo.de>  Sat, 15 Sep 2012 22:31:23 +0200

fonts-sil-abyssinica (1.200-4) unstable; urgency=low

  * Team upload
  * Compress the package with xz
  * Changed fonts-directory to fonts-sil-abyssinica

 -- Sebastian Bator <eremit@posteo.de>  Fri, 14 Sep 2012 15:51:32 +0200

fonts-sil-abyssinica (1.200-3) unstable; urgency=low

  * Team upload
  * Drop Suggests on x-ttcidfont-conf. Closes: #660056
  * Update Standards to 3.9.3 (checked)
  * Change priority of transitional package to extra

 -- Christian Perrier <bubulle@debian.org>  Sun, 26 Feb 2012 19:42:58 +0100

fonts-sil-abyssinica (1.200-2) unstable; urgency=low

  * Team upload
  * Fix Replaces and Conflicts

 -- Christian Perrier <bubulle@sesostris.kheops.frmug.org>  Sat, 05 Nov 2011 18:48:58 +0100

fonts-sil-abyssinica (1.200-1) unstable; urgency=low

  * Team upload

  [ Nicolas Spalinger ]
  * New upstream release (under OFL 1.1)
  * Package renaming
  * DEP5-style metadata

  [ Christian Perrier ]
  * Bump debhelper compatibility to 8
  * Bump Standards to 3.9.2 (checked)
  * Change fonts install directory from
    usr/share/fonts/truetype/ttf-sil-abyssinica-* to
    usr/share/fonts/truetype/abyssinica
  * Drop defoma transition script (preinst) as the package in
    stable already transitioned
  * Add README.source
  * Use dh7-style rules file

 -- Christian Perrier <bubulle@sesostris.kheops.frmug.org>  Mon, 31 Oct 2011 06:08:29 +0100

ttf-sil-abyssinica (1.0-9) unstable; urgency=low

  [ Christian Perrier ]
  * Use "fontforge-nox - fontforge" in Build-Depends
  * Update Standards to 3.9.1 (checked)

  [ Davide Viti ]
  * strip_glyphs.pe: Re-encode files as "unicodeFull".
    Closes: #547577

 -- Christian Perrier <bubulle@debian.org>  Sun, 13 Feb 2011 12:17:18 +0100

ttf-sil-abyssinica (1.0-8) unstable; urgency=low

  * Drop x-ttcidfont-conf, fontconfig et al. from Suggests
  * Fix bashism in debian/rules. Closes: #581475

 -- Christian Perrier <bubulle@debian.org>  Thu, 13 May 2010 07:35:15 +0200

ttf-sil-abyssinica (1.0-6) unstable; urgency=low

  * Change the logic in preinst script to be really sure that we
    remove the defoma hints file

 -- Christian Perrier <bubulle@debian.org>  Wed, 02 Dec 2009 05:41:03 +0100

ttf-sil-abyssinica (1.0-5) unstable; urgency=low

  * Bump debhelper compatibility to 7
  * Switch to 3.0 (quilt) source format
  * Add ${misc:Depends} to dependencies to properly cope with
    debhelper-triggerred dependencies
  * Add myself as Uploader
  * Update Standards to 3.8.3 (checked)
  * Drop defoma use
  * Replace "dh_clean -k" by "dh_prep"

 -- Christian Perrier <bubulle@debian.org>  Sun, 29 Nov 2009 22:18:23 +0100

ttf-sil-abyssinica (1.0-4) unstable; urgency=low

  * Added fontforge script to strip the uneeded glyphs to reduce the d-i
    footprint. The udeb font is also renamed and this is reflected in
    the metadata (Closes: #460945). Thanks to Christian Perrier for his
    patch and to Davide Viti for his fontforge script.

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sun, 27 Jan 2008 20:19:40 +0100

ttf-sil-abyssinica (1.0-3) unstable; urgency=low

  * Bump up Standards-Version to 3.7.3.
  * Add udeb stanza to enable Amharic in d-i. Thanks to Christian Perrier
    for his patch. (Closes: #457481).
  * Drop the Author: optional field.
  * Add the VCS fields.
  * Drop unneeded Recommends (Closes: #439305).
  * Bump debhelper compatibility to version 5.
  * Update upstream homepage and move to new header (Closes: #429274).
  * Install FONTLOG.txt as the usual changelog.
  * Remove extra license files.
  * Make reportbug script executable.
  (Thanks to Lior Kaplan for his help).

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sun,  31 Dec 2007 16:30:21 +0100

ttf-sil-abyssinica (1.0-2) unstable; urgency=low

  * debian/control:
    - Moved to co-maintainership via the Debian Fonts Task Force
      (Alioth pkg-fonts project).
    - Added Daniel Glassey as Uploader.
    - Updated the Suggests for the new version of Graphite.
    - Bumped up Standards-version.
    - Fixed package descriptions.
  * Fixed font name & family in defoma-hints.
  * Cleaned up debian/rules.

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Fri,  8 Jun 2007 00:34:21 +0100

ttf-sil-abyssinica (1.0-1) unstable; urgency=low

  * Initial release under the OFL (Open Font License) version 1.0

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Thu,  22 Jun 2006 15:15:25 +0100
